const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type: Number,
		required: [true, "Required"]
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},
	orderUserId : {
		type: String
	},
	orderProducts : {
		type: Array
	}
	
});

module.exports = mongoose.model("Order", orderSchema);

